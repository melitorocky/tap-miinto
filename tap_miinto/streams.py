"""Stream type classes for tap-miinto."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_miinto.client import miintoStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class OrdersStream(miintoStream):
    """Define custom stream."""
    name = "orders"
    path = "/orders"
    primary_keys = ["id"]
    replication_key = "createdAt"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        # Order - Tap Specific Infos
        th.Property("shopId", th.StringType),
        th.Property("shopLabel", th.StringType),
        th.Property("shopRole", th.StringType),
        # Order - Miinto
        th.Property("id", th.IntegerType),
        th.Property("status", th.StringType),
        th.Property("parentOrder",
            th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("createdAt", th.DateTimeType),
            ),
        ),
        th.Property("createdAt", th.DateTimeType),
        th.Property("currency", th.StringType),
        th.Property("billingInformation",
            th.ObjectType(
                th.Property("name", th.StringType),
                th.Property("email", th.StringType),
                th.Property("phone", th.StringType),
                th.Property("address",
                    th.ObjectType(
                        th.Property("street", th.StringType),
                        th.Property("postcode", th.StringType),
                        th.Property("city", th.StringType),
                        th.Property("country", th.StringType),
                        th.Property("countryCode", th.StringType),
                    ),
                ),
            ),
        ),
        th.Property("shippingInformation",
            th.ObjectType(
                th.Property("deliveryAddress",
                    th.ObjectType(
                        th.Property("type", th.StringType),
                        th.Property("company", th.StringType),
                        th.Property("name", th.StringType),
                        th.Property("email", th.StringType),
                        th.Property("phone", th.StringType),
                        th.Property("address",
                            th.ObjectType(
                                th.Property("street", th.StringType),
                                th.Property("postcode", th.StringType),
                                th.Property("city", th.StringType),
                                th.Property("country", th.StringType),
                                th.Property("countryCode", th.StringType),
                            ),
                        ),
                        th.Property("parcelShopNumber", th.StringType),
                    ),
                    th.Property("deliveryOptions",
                        th.ObjectType(
                            th.Property("initial",
                                th.ObjectType(
                                    th.Property("providerId", th.StringType),
                                ),
                            ),
                            th.Property("override",
                                th.ObjectType(
                                    th.Property("providerId", th.StringType),
                                ),
                            ),
                        ),
                    ),
                    # th.Property("deliveryInfo",
                    #     th.ObjectType(
                    #         th.Property("canUseShippingService", th.BooleanType),
                    #         th.Property("trackingNumber", th.StringType),
                    #         th.Property("price", th.NumberType),
                    #         th.Property("isInternational", th.StringType),
                    #     ),
                    # ),
                    th.Property("status", th.StringType),
                ),
            ),
        ),
        th.Property("pendingPositions",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("quantity", th.IntegerType),
                    th.Property("stock", th.IntegerType),
                    th.Property("physicalStock", th.IntegerType),
                    th.Property("price", th.NumberType),
                    th.Property("item",
                        th.ObjectType(
                            th.Property("styleId", th.StringType),
                            th.Property("name", th.StringType),
                            th.Property("gtin", th.StringType),
                            th.Property("brand", th.StringType),
                            th.Property("productNumber", th.StringType),
                            th.Property("productId", th.StringType),
                            th.Property("miintoItemId", th.StringType),
                            th.Property("color", th.StringType),
                            th.Property("size", th.StringType),
                            th.Property("imageUrls",
                                th.ArrayType(
                                    th.StringType
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
        th.Property("acceptedPositions",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("quantity", th.IntegerType),
                    th.Property("stock", th.IntegerType),
                    th.Property("physicalStock", th.IntegerType),
                    th.Property("price", th.NumberType),
                    th.Property("item",
                        th.ObjectType(
                            th.Property("styleId", th.StringType),
                            th.Property("name", th.StringType),
                            th.Property("gtin", th.StringType),
                            th.Property("brand", th.StringType),
                            th.Property("productNumber", th.StringType),
                            th.Property("productId", th.StringType),
                            th.Property("miintoItemId", th.StringType),
                            th.Property("color", th.StringType),
                            th.Property("size", th.StringType),
                            th.Property("imageUrls",
                                th.ArrayType(
                                    th.StringType
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
        th.Property("declinedPositions",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("quantity", th.IntegerType),
                    th.Property("stock", th.IntegerType),
                    th.Property("physicalStock", th.IntegerType),
                    th.Property("price", th.NumberType),
                    th.Property("item",
                        th.ObjectType(
                            th.Property("styleId", th.StringType),
                            th.Property("name", th.StringType),
                            th.Property("gtin", th.StringType),
                            th.Property("brand", th.StringType),
                            th.Property("productNumber", th.StringType),
                            th.Property("productId", th.StringType),
                            th.Property("miintoItemId", th.StringType),
                            th.Property("color", th.StringType),
                            th.Property("size", th.StringType),
                            th.Property("imageUrls",
                                th.ArrayType(
                                    th.StringType
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
        th.Property("dateBalanced", th.StringType),
        th.Property("context",
            th.ArrayType(
                th.StringType,
            ),
        ),
    ).to_dict()


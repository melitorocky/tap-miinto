"""Custom client handling, including miintoStream base class."""

import requests,json, psycopg2
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable, cast

from .class_orders import Orders

from singer_sdk.streams import Stream

from datetime import datetime
from datetime import timedelta
from dateutil import parser
import pendulum

import pprint
pp = pprint.PrettyPrinter(indent=2)

import logging
LOGGER = logging.getLogger(__name__)

class miintoStream(Stream):
    """Stream class for Miinto streams."""

    # State fix
    is_sorted = True

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        streamName = type(self).__name__

        # map the inputs to the function blocks
        if streamName == 'OrdersStream':
            return self.OrdersStream(context)
        else:
            print(streamName + ' not found..')
            quit()


    def get_credentials(self):
        credentials = {
            "identifier" : self.config["identifier"],
            "secret" : self.config["secret"],
            "authApiURL" : self.config["authApiURL"],
            "orderApiURL" : self.config["orderApiURL"],
            "maxNumOrders" : self.config["maxNumOrders"],
            "bookmark" : self.config["bookmark"],
            "start_date" : self.config["start_date"],
        }
        return credentials


    def OrdersStream(self,context):
        finalRes = []

        credentials = self.get_credentials()
        maxNumOrders = credentials['maxNumOrders']

        OrderClass = Orders(credentials=credentials)
        authResponse = OrderClass.authorize()

        # Sign the request for Order API
        responseContent = json.loads(authResponse.text)
        responseData = responseContent["data"]

        dictStoreIds = OrderClass.get_store_ids(privileges=responseData["privileges"])

        # quit(f"\n\n{dictStoreIds}\n\n")

        for shopLabel,dictRoles in dictStoreIds.items():
            for shopRole,listShopIds in dictRoles.items():
                for shopId in listShopIds:
                    # getOrders
                    # NOTE: no "updatedAt" field -> need to get ALL orders everytime
                    listOrders = OrderClass.get_all_orders(
                        identifier = responseData["id"],
                        token = responseData["token"],
                        shopId = shopId,
                        maxNumOrders = maxNumOrders
                    )

                    if listOrders:
                        for order in listOrders:
                            orderFull = order
                            orderFull["shopId"] = shopId
                            orderFull["shopLabel"] = shopLabel
                            orderFull["shopRole"] = shopRole
                            finalRes.append(orderFull)

        # Reordering for createdAt, since we have a mix of all the different shopId
        finalRes = sorted(finalRes, key=lambda k: k['createdAt']) 
        
        # print("\n\n----------------------------\n\n")
        # pp.pprint(finalRes)
        # print("\n\n----------------------------\n\n")
        # quit()

        return finalRes

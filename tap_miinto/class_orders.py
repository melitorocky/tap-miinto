import requests, json, time, random, hmac, hashlib
from urllib.parse import urlparse, urlencode

import logging
LOGGER = logging.getLogger(__name__)

class Orders:
    identifier = None
    secret = None
    authApiURL = None
    orderApiURL = None
    maxNumOrders = None
    start_date = None

    def __init__(self, credentials=None):
        self.identifier = credentials["identifier"]
        self.secret = credentials["secret"]
        self.authApiURL = 'https://' + credentials["authApiURL"]
        self.orderApiURL = 'https://' + credentials["orderApiURL"]
        self.maxNumOrders = credentials['maxNumOrders']
        self.start_date = credentials['start_date']

    
    def authorize(self):
        endpoint = self.authApiURL + "/channels"

        headers = {
            "Content-Type": "application/json",
        }

        body = {
            "identifier": self.identifier,
            "secret": self.secret
        }

        response = requests.post(
            endpoint,
            headers=headers,
            data=json.dumps(body)
        )

        # Check if positive response
        if response.status_code != 200:
            print("OH NO, NON MI AUTENTICO!")
            print(f"Codice: {response.status_code} - Errore: {response.text}")
            quit()
        
        return response
    

    # NOTE: This function only support 1 value per "BrandAdmin".
    # Patch here if/when needed.
    def get_store_ids(self, privileges):
        dictStoreIds = {}

        for key,val in privileges.items():
            if key != "__GLOBAL__":
                dictStoreIds[key] = {}
                for shopRole,dictRoles in val.items():
                    dictStoreIds[key][shopRole] = []
                    for shopId,shopPermissions in dictRoles.items():
                        dictStoreIds[key][shopRole].append(shopId)
        
        return dictStoreIds
    

    def get_orders(self, **kwargs):
        dictOrders = {}
        dictQueryParams = kwargs["dictQueryParams"]
        identifier = kwargs["identifier"]
        token = kwargs["token"]
        shopId = kwargs["shopId"]

        queryParams = urlencode(dictQueryParams)
        url = self.orderApiURL + "/shops/" + shopId + "/orders?" + queryParams
        anchor = urlparse(url)
        query = anchor.query.replace('?', "")
        resource = "GET" + "\n" + anchor.hostname + "\n" + anchor.path + "\n" + query
        resourceSignature = hashlib.sha256(resource.encode('utf-8')).hexdigest()
        timestamp = str(int(time.time()))
        authSeed = str(int(time.time()) * 1000 + random.randint(0, 100))
        authType = "MNT-HMAC-SHA256-1-0"
        
        headerString = identifier + "\n" + timestamp + "\n" + authSeed + "\n" + authType
        headerSignature = hashlib.sha256(headerString.encode('utf-8')).hexdigest()
        payloadString = ""
        payloadSignature = hashlib.sha256(payloadString.encode('utf-8')).hexdigest()
        requestString = resourceSignature + "\n" + headerSignature + "\n" + payloadSignature

        signature = hmac.new(token.encode('utf-8'), requestString.encode('utf-8'), hashlib.sha256).hexdigest()

        osHeaders = {
            'Miinto-Api-Auth-ID' : identifier,
            'Miinto-Api-Auth-Signature' : signature,
            'Miinto-Api-Auth-Timestamp' : timestamp,
            'Miinto-Api-Auth-Seed' : authSeed,
            'Miinto-Api-Auth-Type' : authType,
            'Miinto-Api-Control-Version' : '2.6',
            # 'Miinto-Api-Control-Flavour' : 'Miinto-Generic',
            # 'Content-Type' : 'application/json'
        }

        # import pprint
        # pprint.pprint(osHeaders)
        # quit("\n\n___\n\n")

        # // There are more attributes to use in a call, just change the endpoint adress:
        # // https://api-order.miinto.net/shops/shopId/orders?status%5B%5D=accepted&awaitingTrackingNumber=false&limit=50&offset=0&sort=-createdAt&query=text
        endpoint = url

        # make a request to Order API
        response = requests.get(
            endpoint,
            headers=osHeaders
        )

        # Check if positive response
        if response.status_code == 200:
            dictOrders = json.loads(response.text)
        else:
            # NOTE: We get a 403 error on some shopIds because programmer's life is full of fun
            LOGGER.warning("\n\n!!!OH NO, NON ACCEDO AGLI ORDINI!\n\n")
            LOGGER.warning(f"Codice: {response.status_code} - Errore: {response.text}")
            dictOrders = json.loads(response.text)
            # print("OH NO, NON ACCEDO AGLI ORDINI!")
            # print(f"Codice: {response.status_code} - Errore: {response.text}")
            # quit()

        return dictOrders

    def get_all_orders(self, **kwargs):
        listOrders = []
        maxNumOrders = kwargs["maxNumOrders"]

        dictQueryParams = {
            # "status[]" : "accepted",
            # "awaitingTrackingNumber" : "false",
            "limit" : maxNumOrders,
            "offset" : 0,
            "sort" : "-createdAt",
            # "sort" : "+createdAt",
        }

        identifier = kwargs["identifier"]
        token = kwargs["token"]
        shopId = kwargs["shopId"]

        listOrdersResult = self.get_orders(
            dictQueryParams = dictQueryParams,
            identifier = identifier,
            token = token,
            shopId = shopId,
            maxNumOrders = maxNumOrders
        )

        listOrders = listOrdersResult["data"]

        # Check if there is a pagination
        if "totalItemCount" in listOrdersResult["meta"]:
            totalItemCount = int(listOrdersResult["meta"]["totalItemCount"])
            if totalItemCount > maxNumOrders:
                # Note: the first iteration has already been done
                iterationsLeft = (totalItemCount // maxNumOrders + bool(totalItemCount % maxNumOrders)) - 1

                for iteration in range(iterationsLeft):
                    offset = maxNumOrders * (iteration + 1)
                    # print(f"ITERAZIONE: {iteration}/{iterationsLeft} => offset: {offset}\n")

                    listOrdersPaged = self.get_orders(
                        dictQueryParams = dictQueryParams,
                        identifier = identifier,
                        token = token,
                        shopId = shopId,
                        maxNumOrders = maxNumOrders,
                        offset = offset
                    )

                    listOrders.extend(listOrdersPaged["data"])
        # ELSE: no order, or there was an error (403 on specific shopIds)

        # import pprint
        # print("\n\n__________________________________________\n\n")
        # pprint.pprint(listOrders)
        # print("\n\n__________________________________________\n\n")
        # quit()

        return listOrders